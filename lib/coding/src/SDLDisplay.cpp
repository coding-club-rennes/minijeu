/*
** SDLDisplay.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:19:41 2015 Alexis Bertholom
// Last update Tue Nov 21 19:58:43 2017 Lucas
*/

#include "SDLDisplay.hpp"
#include "SDLError.hpp"

SDLDisplay::SDLDisplay(std::string const& winTitle, Uint winW, Uint winH,
                       Uint winX, Uint winY)
    : SDLContext(SDL_INIT_VIDEO), _winW(winW), _winH(winH) {
  SDL_Surface* screen;

  if (!(this->_window =
            SDL_CreateWindow(winTitle.c_str(), winX, winY, winW, winH, 0)))
    throw(SDLError("SDL window creation failed"));
  if (!(screen = SDL_GetWindowSurface(this->_window))) throw(SDLError());
  this->_screen = new Image(screen, Image::Wrap);
}

SDLDisplay::~SDLDisplay() {
  SDL_DestroyWindow(this->_window);
  delete (this->_screen);
}

void SDLDisplay::putPixel(Uint x, Uint y, Color color) {
  this->_screen->putPixel(x, y, color);
}

void SDLDisplay::putRect(Uint x, Uint y, Uint w, Uint h, Color color) {
  this->_screen->putRect(x, y, w, h, color);
}

void SDLDisplay::putImage(Uint x, Uint y, Image& img, Uint w, Uint h) {
  this->_screen->blit(x, y, img, w, h);
}

void SDLDisplay::putText(std::string const& msg, Uint x, Uint y, Color color) {
  this->_screen->putText(msg, x, y, color);
}

void SDLDisplay::putImage(std::string const& path, Uint x, Uint y) {
  this->_screen->putImage(path, x, y);
}

void SDLDisplay::clearScreen() { this->_screen->clear(); }

void SDLDisplay::fillScreen(Color color) { this->_screen->fill(color); }

void		SDLDisplay::putGrid(int tab[8][8], int x, int y)
{
  this->_screen->putGrid(tab, x, y);
}

void SDLDisplay::refreshScreen() { SDL_UpdateWindowSurface(this->_window); }

Uint SDLDisplay::getWinW() const { return (this->_winW); }

Uint SDLDisplay::getWinH() const { return (this->_winH); }
