/*
** SDLContext.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:05:14 2015 Alexis Bertholom
** Last update Tue Jan 27 12:05:51 2015 Alexis Bertholom
*/

#ifndef SDLCONTEXT_HPP_
# define SDLCONTEXT_HPP_

# include "types.hpp"

class		SDLContext
{
public:
  SDLContext(Uint32 flags);
  ~SDLContext();

public:
  bool		isInitialized() const;
};

#endif
